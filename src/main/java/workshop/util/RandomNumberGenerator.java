package workshop.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.SecureRandom;
import java.util.HashSet;

public class RandomNumberGenerator {

    public static void main(String[] args) throws IOException {

        final var random = new SecureRandom();

        Path path = Paths.get("C:/Development/Files/Output/random_number_7_digits.txt");

        verify(path);

        var numbers = new HashSet<String>();

        for (int i = 0; i < 20_000_000; i++) {
            int number = 1000000 + random.nextInt(9000000);
            numbers.add(String.valueOf(number));
        }
        System.out.println("size of list: " + numbers.size());
        System.out.println("starting file writing...");
        Files.write(path, numbers, StandardOpenOption.CREATE);
        System.out.println("completed file writing");
    }

    private static void verify(Path path) throws IOException {
        Files.deleteIfExists(path);
        Files.createFile(path);
    }
}
