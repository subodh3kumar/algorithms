package workshop.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SortNumber {

    public static void main(String[] args) throws IOException {
        Path path = Path.of("C:/Development/Files/Output/random_number_7_digits.txt");
        List<Integer> numbers = Files.readAllLines(path).stream().map(Integer::valueOf).toList();
        System.out.println("number list size: " + numbers.size());
    }
}
