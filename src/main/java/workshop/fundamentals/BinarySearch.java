package workshop.fundamentals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch {

    private static final String FILE_PATH = "C:/Development/Files/Input/Data/tinyW.txt";

    public static void main(String[] args) {

        int[] numbers = getNumbers();
        System.out.println(Arrays.toString(numbers));

        Arrays.sort(numbers);

        Scanner input = new Scanner(System.in);
        System.out.print("enter a number: ");
        int key = input.nextInt();

        int number = binarySearch(numbers, key);

        if (number != -1) {
            System.out.println("search value is available");
        } else {
            System.out.println("search value is not available");
        }
    }

    private static int binarySearch(int[] numbers, int key) {

        int start = 0;
        int end = numbers.length - 1;

        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (key < numbers[mid]) {
                end = mid - 1;
            } else if (key > numbers[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    private static int[] getNumbers() {
        try {
            return Files.readAllLines(Path.of(FILE_PATH))
                    .stream()
                    .mapToInt(Integer::parseInt)
                    .toArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new int[0];
    }
}
